##############################
#
#   RPI bar code reader
#   @author github.com/john-e
#
#   @lib
#       zbar - https://github.com/npinchot/zbar
##############################

import sys
import io
import time
import picamera
from PIL import Image
import zbar
import request_library;
import RPi.GPIO as GPIO
from request_library import request_lib

# Buzzer and Light Code

buzzer_gpio = 5
led_green_gpio = 18
led_red_gpio = 17
led_blue_gpio = 6

gpio_array = [buzzer_gpio,led_green_gpio,led_red_gpio, led_blue_gpio]

GPIO.setmode(GPIO.BCM)

for gpio in gpio_array:
    GPIO.setup(gpio,GPIO.OUT)

def beep(gpio, GPIO):
    GPIO.output(gpio, True)
    time.sleep(1)
    GPIO.output(gpio, False)
    time.sleep(1)

def two_beep(gpio, GPIO):
    beep(gpio, GPIO)
    beep(gpio, GPIO)

def light_on(color, GPIO):
    led_green_gpio = 18
    led_red_gpio = 17
    led_blue_gpio = 6
    buzzer_gpio = 5
    gpio = led_green_gpio;
    if color == 'red':
        gpio = led_red_gpio
    elif color == 'green':
        gpio = led_green_gpio
    elif color == 'blue':
        gpio = led_blue_gpio

    GPIO.output(gpio, True)
    if color == 'red':
        two_beep(buzzer_gpio, GPIO)
    elif color == 'green':
        beep(buzzer_gpio, GPIO)
    time.sleep(0.2)
    GPIO.output(gpio, False)

# END of buzzer code

# Create the in-memory stream
stream = io.BytesIO()
# create a reader
scanner = zbar.ImageScanner()
# configure the reader
scanner.parse_config('enable')

with picamera.PiCamera() as camera:
    #configure camera
    camera.video_stabilization = True
    camera.sharpness = 50
    camera.contrast = 30

    #start preview window
    camera.start_preview()

    #initialize stream reader
    stream = io.BytesIO()
    try:
        for foo in camera.capture_continuous(stream, format='jpeg'):
            # Truncate the stream to the current position (in case
            # prior iterations output a longer image)
            stream.truncate()
            stream.seek(0)

            # obtain image data
            pil = Image.open(stream).convert('L')
            width, height = pil.size
            raw = pil.tobytes()

            # wrap image data
            image = zbar.Image(width, height, 'Y800', raw)

            # scan the image for barcodes
            scanner.scan(image)

            # extract results
            for symbol in image:
                # do something useful with results
                print 'decoded', symbol.type, 'symbol', '"%s"' % symbol.data
                data= '{ "cat_id": 1,"product_id":"'+symbol.data+'"}'
                client = request_lib.request_lib()
                print client.send_post_request('api/getProduct',data)
                light_on('green', GPIO)
                GPIO.cleanup()
            # clean up
            del(image)

            #sleep to avoid 100% cpu usage
            time.sleep(0.05)
    finally:
        camera.stop_preview()
