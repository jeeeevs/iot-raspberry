import time
import RPi.GPIO as gpio
gpio_port = 5

gpio.setwarnings(False)
gpio.setmode(gpio.BOARD)
gpio.setup(gpio_port,gpio.OUT)


try:
    while True:
        gpio.output(gpio_port,0)
        time.sleep(.3)
        gpio.output(gpio_port,1)
        time.sleep(.3)
except KeyboardInterrupt:
    gpio.cleanup()
    exit