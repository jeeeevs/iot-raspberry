import RPi.GPIO as GPIO
import time
import sys


class Buzzer:
    buzzer_gpio_port = 5
    beep_interval = .5

    def __init__(self):
        pass

    def beep(self):
        GPIO.output(self.buzzer_gpio_port, True)
        time.sleep(self.beep_interval)
        GPIO.output(self.buzzer_gpio_port, False)
        time.sleep(self.beep_interval)
