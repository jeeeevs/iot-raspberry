import requests


class request_lib:
    def __init__(self):
        self.service_url = 'http://ec2-54-169-91-148.ap-southeast-1.compute.amazonaws.com'

    def send_post_request(self,api_url, data):
        return requests.post(self.service_url + "/" + api_url, data)
