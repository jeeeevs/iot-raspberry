
from sys import argv
import zbar


def show_scanned_result():
    for symbol in processor.results:
        print 'Type', symbol.type, 'symbol', '"%s"' % symbol.data
# create a Processor
processor = zbar.Processor()

# configure the Processor
processor.parse_config('enable')

# initialize the Processor
device = '/dev/video0'
if len(argv) > 1:
    device = argv[1]
processor.init(device)

# enable the preview window
processor.visible = True

# read at least one barcode (or until window closed)
processor.process_one()


# hide the preview window
processor.visible = False

# extract results
show_scanned_result()


