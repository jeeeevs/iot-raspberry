#!/usr/bin/python
# Import required libraries
import RPi.GPIO as GPIO
import time

# Tell GPIO library to use GPIO references
GPIO.setmode(GPIO.BCM)

# List of LED GPIO numbers
ledGreen = 5
ledRed = 18

GPIO.setup(ledGreen, GPIO.OUT)
GPIO.output(ledGreen, False)
# Set up the GPIO pins as outputs and set False
print "Setup LED pins as outputs"
GPIO.output(ledGreen, True)
time.sleep(0.2)

GPIO.setup(ledRed, GPIO.OUT)
GPIO.output(ledRed, False)
# Set up the GPIO pins as outputs and set False
print "Setup LED pins as outputs"
GPIO.output(ledRed, True)
time.sleep(0.2)

raw_input('led, press enter to exit program')
GPIO.cleanup()