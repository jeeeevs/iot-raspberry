import RPi.GPIO as GPIO
import time

buzzer_gpio = 5
led_green_gpio = 6
led_red_gpio = 6
led_blue_gpio = 6

gpio_array = [buzzer_gpio,led_green_gpio,led_red_gpio, led_blue_gpio]

GPIO.setmode(GPIO.BCM)

for gpio in gpio_array:
    GPIO.setup(gpio,GPIO.OUT)

def beep(self, gpio, GPIO):
    GPIO.output(gpio, True)
    time.sleep(1)
    GPIO.output(gpio, False)

beep(buzzer_gpio, GPIO)
