<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::post('/add-to-cart', [
     'as' => 'add_to_cart',
     'uses' => 'CartItemController@addToCart',
 ]);

Route::get('/cart-items', [
     'as' => 'cart_items',
     'uses' => 'CartItemController@listItems',
 ]);

Route::get('/view-cart-items', [
     'as' => 'cart_items_view',
     'uses' => 'CartItemController@listItemsView',
 ]);

Route::get('/mobile/{cartId}', 'CartItemController@mobileView');