<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hi', function () {
    return "Hello";
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->post('/login', 'App\Http\Controllers\AuthenticateController@authenticate');
    $api->post('/getProduct', 'App\Http\Controllers\ProductController@insertProduct');
});

$api->version('v1', ['middleware' => 'jwt.auth'], function ($api) {
    $api->get('/in', 'App\Http\Controllers\AuthenticateController@getUser');
});
