<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\JsonApiSerializer;
use League\Fractal\Serializer\ArraySerializer;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $request;
    protected $apiAuth;

    /**
     * Default constructor
     */
    public function __construct()
    {
        $this->request = app('Illuminate\Http\Request');
        $this->apiAuth = app('Dingo\Api\Auth\Auth');
        $requestBody = $this->request->getContent();
        $data = json_decode($requestBody);
        if (!is_object($data)) {
            $data = $this->request;
        }
        $this->data = $data;
        
    }
}
