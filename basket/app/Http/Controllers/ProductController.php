<?php
namespace App\Http\Controllers;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Routing\Helpers;
use \App\Models\User;
use App\Repositories\Interfaces\ProductRepositoryInterface;

class ProductController extends Controller
{
    use Helpers;

    public function __construct(ProductRepositoryInterface $productRepo)
    {
        parent::__construct();
        $this->productRepo = $productRepo;
    }
    public function insertProduct()
    {
        $input = [
            'cart_id' => $this->data->cat_id,
            'product_id' => $this->data->product_id
            ];

        if ($this->productRepo->saveProduct($input)) {
            return [
                "status" => "Success"
                ];
        }

    }

    public function getUser()
    {
      
    }
}
