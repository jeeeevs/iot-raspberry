<?php
namespace App\Http\Controllers;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use \App\Models\User;

class AuthenticateController extends Controller
{
    public function authenticate()
    {

        // grab credentials from the request
        $credentials['email'] = $this->data->email;
        $credentials['password'] = $this->data->password;
        // $credentials = \App\Models\User::first();
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        
        // all good so return the token
        return response()->json(compact('token'));
    }

    public function getUser()
    {
        $user = $this->apiAuth->user();

        return json_encode($user);
    }
}
