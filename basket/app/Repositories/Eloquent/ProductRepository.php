<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Models\Product;
use DB;

class ProductRepository implements ProductRepositoryInterface
{
    public function __construct(Product $product)
    {
        $this->product = $product;
    }
    public function saveProduct($input)
    {
        return $this->product->create($input);
    }
}